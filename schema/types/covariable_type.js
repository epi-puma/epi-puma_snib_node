const { gql } = require('apollo-server');

module.exports = gql`
  type CovariableSNIB implements Covariable @key(fields: "id") {
	id: ID!,
  	reinovalido: String,
	phylumdivisionvalido: String,
	clasevalida: String,
	ordenvalido: String,
	familiavalida: String,
	generovalido: String,
	especievalida: String,
	cells_state: [String!]!,
	cells_mun: [String!]!,
	cells_ageb: [String!]!
  }
`;
