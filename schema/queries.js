const { gql } = require('apollo-server');


const schema = gql`
  extend type Query {

    echo(message: String): String,
    all_snib_covariables(limit: Int!, filter: String!): [CovariableSNIB!]!
    ocurrence_snib_by_id_covariable(id_covariable: Int!, filter: String!): [OcurrenceSNIB!]!
  
  }
`;

module.exports = schema;